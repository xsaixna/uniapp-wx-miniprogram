export default {
	// 添加商品
	setCartInfo(state, payload) {
		// find函数返回的是找到的元素或者是undefined
		const cartInfo = state.cartInfoList.find((val) => {
			return val.goods_id === payload.cartInfo.goods_id
		})
		if (!cartInfo) {
			state.cartInfoList.push(payload.cartInfo)
		} else {
			cartInfo.goods_count++
		}
		this.commit("saveStateToStorage",{key:"cartInfoList",content:"cartInfoList"})
		// console.log(state.cartInfoList)
	},
	// 保存商品列表
	saveStateToStorage(state,payload) {
		// console.log(...[payload.key])
		// uni.setStorageSync("cartInfoList", JSON.stringify(state.cartInfoList))
		uni.setStorageSync(...[payload.key], JSON.stringify(state[payload.content]))
	},
	// 更新商品
	updateGoodsInfo(state, payload) {
		// 更新是否被选中
		let goodsInfo = state.cartInfoList.find(val => {
			return val.goods_id === payload.item.goods_id
		})
		if (payload.flag === "count") {
			goodsInfo.goods_count = payload.item.goods_count
		} else if (payload.flag === "state") {
			goodsInfo.goods_state = !payload.item.goods_state
		}
		this.commit("saveStateToStorage",{key:"cartInfoList",content:"cartInfoList"})
	},
	// 删除商品
	deleteCartInfo(state, payload) {
		state.cartInfoList = state.cartInfoList.filter(val => {
			return payload.cartInfo.goods_id !== val.goods_id
		})
		this.commit("saveStateToStorage",{key:"cartInfoList",content:"cartInfoList"})
	},
	setUserAddress(state, payload){
		console.log(payload)
		if(payload.userAddress===null){
			return
		}
		state.userAddressList.push(payload.userAddress)
		this.commit("saveStateToStorage",{key:"userAddressList",content:"userAddressList"})
	},
	// 全选
	changeStateGoods(state, payload){
		state.cartInfoList.forEach(val=>{
			val.goods_state=payload.isDisabled
		})
		this.commit("saveStateToStorage",{key:"cartInfoList",content:"cartInfoList"})
	},
	// 存储用户信息
	setUserInfo(state,userInfo){
		state.userInfo=userInfo
		this.commit("saveStateToStorage",{key:"userInfo",content:"userInfo"})
	},
	setToken(state,token){
		state.token=token
		this.commit("saveStateToStorage",{key:"token",content:"token"})
	},
	deleteToken(state){
		state.token=0
		this.commit("saveStateToStorage",{key:"token",content:"token"})
	},
	deleteUserInfo(){
		uni.removeStorageSync("userInfo")
	},
	deleteUserAddress(){
		uni.removeStorageSync("userAddressList")
	},
	setRedirectInfo(state,redirectInfo){
		state.redirectInfo=redirectInfo
	},
	updateRedirectInfo(state){
		state.redirectInfo=null
	}
}
