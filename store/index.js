import Vue from 'vue';
import Vuex from 'vuex';

import actions from "./actions.js"
import getters from "./getters.js"
import mutations from "./mutations.js"

Vue.use(Vuex)

const store=new Vuex.Store({
	state:{
		// 购物车消息
		/* cartInfo:{
			goods_id:"",
			goods_name:"",
			goods_price:"",
			goods_count:"",
			goods_small_logo:"",
			goods_state:false//商品是否被选中
		} */
		cartInfoList:JSON.parse(uni.getStorageSync("cartInfoList")||'[]'),
		userAddressList:JSON.parse(uni.getStorageSync("userAddressList")||'[]'),
		userInfo:JSON.parse(uni.getStorageSync("userInfo")||'{}'),
		token:uni.getStorageSync("token")||'',
		redirectInfo:null,//重定向
	},
	actions,
	getters,
	mutations
})



export default store