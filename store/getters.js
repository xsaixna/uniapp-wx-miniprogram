export default {
	getCartInfoList(state) {
		return state.cartInfoList
	},
	getTotalGoods(state) {
		let count = 0
		state.cartInfoList.forEach(val => {
			count += val.goods_count
			
		})
		return count
	},
	getUserAddress(state) {
		return state.userAddressList
	},
	selectGoodsTotalPrice(state) {
		// console.log(state.cartInfoList.filter(val => val.goods_state),"+++++")
		 let selectGoodsList=state.cartInfoList.filter(val => val.goods_state)
		 let totalPrice=selectGoodsList.reduce((total, item) => {
			return total += parseInt(item.goods_price)*parseInt(item.goods_count)
		}, 0)
		return [selectGoodsList.length,totalPrice]
	},
	getUserToken(state){
		 if(state.token!=0){
			 return false
		 }else{
			 return true
		 }
	},
	getUserInfo(state){
		return state.userInfo
	},
	getRedirectInfo(state){
		return state.redirectInfo
	}
}
