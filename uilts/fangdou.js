export default function fangdou(func, time) {
	let timer;
	return function() {
		if (timer) clearTimeout(timer)
		timer = setTimeout(() => {
			func()
		}, time)
	}
}
