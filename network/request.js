// 导入网络请求包
// 从' @escook/request-miniprogram '导入{  $ http }   
import {
	$http
} from "@escook/request-miniprogram"

import store from "@/store/index.js"

// uni-app的顶级对象是uni 就相当于wx
uni.$http = $http

$http.baseUrl = "https://www.uinav.com"

// 请求拦截器
// 请求开始之前做一些事情
$http.beforeRequest = function(options) {
	// 展示loading
	uni.showLoading({
		title: "数据加载中..."
	})
	
	// console.log(store,"store")
	// 判断用户是否登录，只有登录了的用户才有权限支付
	if(options.url.indexOf('/my/')!==-1){
		options.header={
			Authorization:"013pZWCt0GniKb1ftkBt0AxfDt0pZWCf"
		}
		console.log(options)
	}
	
}

// 响应拦截器
$http.afterRequest = function() {
	uni.hideLoading()
}