# 项目截图
## 首页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172319_8676dc60_7804790.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172334_d4abed83_7804790.png "屏幕截图.png")
## 分类页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172349_3a62a6fe_7804790.png "屏幕截图.png")
## 购物车页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172412_f76abd50_7804790.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172431_1af256aa_7804790.png "屏幕截图.png")
## 我的页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172449_d8906fe7_7804790.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172509_36e78f57_7804790.png "屏幕截图.png")
## 登录页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172533_8b06433f_7804790.png "屏幕截图.png")
## 商品列表页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172552_c81abb0a_7804790.png "屏幕截图.png")
## 商品详情页
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172608_5e0bc88f_7804790.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172621_e268f8ae_7804790.png "屏幕截图.png")
## 搜索页
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172630_72b1f38d_7804790.png "屏幕截图.png")
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/172643_d7649ddb_7804790.png "屏幕截图.png")
# 项目笔记
# 1.网络请求依赖包

- 先初始化包管理器
  - npm init -y
  - npm install @escook/request-miniprogram
  - https://www.npmjs.com/package/@escook/request-miniprogram

┌─components            uni-app组件目录
│  └─comp-a.vue         可复用的a组件
├─hybrid                存放本地网页的目录，详见
├─platforms             存放各平台专用页面的目录，详见
├─pages                 业务页面文件存放的目录
│  ├─index
│  │  └─index.vue       index页面
│  └─list
│     └─list.vue        list页面
├─static                存放应用引用静态资源（如图片、视频等）的目录，注意：静态资源只能存放于此
├─wxcomponents          存放小程序组件的目录，详见
├─subpackage						分包目录
├─main.js               Vue初始化入口文件
├─App.vue               应用配置，用来配置App全局样式以及监听 应用生命周期
├─manifest.json         配置应用名称、appid、logo、版本等打包信息，详见
└─pages.json            配置页面路由、导航条、选项卡、分包路径等页面类信息，详见

# 2.获取手机屏幕硬件api

- uni.getSystemInfoSync()
  - screenHeight 屏幕高度
  - windowHeight 可使用屏幕高度

![image-20210730133508517](E:\typora\photo_save_path\image-20210730133508517.png)

# 3.scroll-view设置 scrollTop

scrollTop规定滚动条的位置的，类型时number

# 4.在uni-app中使用组件

- 创建一个组件目录 components
- 然后右击components选择创建组件
- 组件的名称一般设置为 xxx-xxx
- 使用组件
  - 直接在页面中使用组件的名称作为标签即可

# 5.保存数据到本地

```
存储：uni.setStorageSync("存储时设置Key-唯一",JSON.stringify(存储的数据))
	uni.setStorageSync("kw",JSON.stringify(this.searchHistoryList))
取值：uni.getStorageSync(key值)
	JSON.parse(uni.getStorageSync("kw")|| "[]")
删除：
	删除某个值 uni.removeStorageSync(KEY)
	清空本地缓存 uni.clearStorage();
注意：存储到本地的数据要是字符串形式JSON.stringify(数据)==》将js对象转换为字符串
	JSON.parse(数据)==》将字符串数据转换为js对象数据
```

# 6.接收路由参数

在生命周期onLoad(options){}函数中接收路由传递过来的参数，options就是参数对象

# 7.uni-app中开启微信小程序的上拉和下拉

在page.json文件中

![image-20210731153147743](E:\typora\photo_save_path\image-20210731153147743.png)

```
//上拉加载数据事件，页面到达底部就会指向这个钩子函数
onReachBottom(){

},
// 下拉刷新,重置数据
onPullDownRefresh(){

},
```

###  8。预览轮播图的大图

```
// 大图预览Api
uni.previewImage({
        current:index,//从那张图片开始预览,索引值
        urls:this.goodsDetailInfo.pics.map(val=>{
        return val.pics_big
    })//需要预览图片的地址 urls 数组
})
```

# 9.iso手机对webp格式的图片不是特别的支持，如果小程序开发中需要webp格式的图片，将webp改为jpg

# 10.设置tabbar徽标

![image-20210801152927131](E:\typora\photo_save_path\image-20210801152927131.png)

```
uni Api进行设置
uni.setTabBarBadge({
    index:2,//修改第几个tabbar
    text:this.totalGoods.toString()//设置徽标的值，必须是字符串
})
```

# 11单选框与复选框

单选框：radio

# 12左滑

# 13 uni-app中导入第三方组件库

首先将npm包下载到本地，然后再node_modules中找到对应组件的文件夹，找到文件夹中的dist文件夹，移动到自己创建的wxcomponents文件夹，创建一个自定义的文件夹中

引入的时候使用：vue中引入组件的方式即可

import xxx from "@/wxcomponents/dist/xxx/.../index"

详见：https://segmentfault.com/a/1190000022397067

# 14 选择收货地址

**uni Api**

```
uni.chooseAddress()
```

# 15 确定框

uni.showModal()

# 16.开启关闭定位授权

# 17小程序中登录按钮（固定写法）
    <button type="primary" class="btn-login" open-type="getUserInfo" @getuserinfo="getUserInfo">一键登录</button>
    	通过open-type="getUserInfo" @getuserinfo="getUserInfo"可以通过用户授权，获取到用户的基本消息
    	methods:{
    		getUserInfo(event){//event中包含用户的信息
    			
    		}
    	} 
    使用uni.login() 获取用户的登录信息 用户信息中包含了微信用户的昵称，头像。。。
# 18倒计时

