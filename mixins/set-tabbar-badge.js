import { mapGetters } from "vuex"; //将getter中导入到计算属性中
export default {
	computed:{
		...mapGetters({
			totalGoods:"getTotalGoods"
		}),
	},
	// 页面展示时，调用
	onShow() {
		this.setBadge()
	},
	watch:{
		totalGoods(){
			this.setBadge()
		}
	},
	
	methods: {
		setBadge(){
			uni.setTabBarBadge({
				index:2,
				text:this.totalGoods.toString()
			})
		},
	},
}